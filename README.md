<b>!!! WARNING: in the process of cleaning up code, data, and the git in general. If you notice any errors please let us know !!!</b>


Welcome to the CrowdGO gitlab page! CrowdGO is a protein Gene Ontology predictor using a meta approach, analyzing the predictions of other tools in order to get an improved precision and recall.

Please note that the CrowdGO snakemake workflow is currently only tested on Ubuntu. It should work on OSX, but please report any errors to maarten.reijnders@unil.ch or create an issue.

[[_TOC_]]

## Quick start
CrowdGO itself consists of just two python scripts (prediction and training), however creating CrowdGO input requires several other protein function predictors that need to be set up. The most straightforward way to get started with predicting GO terms using CrowdGO is by following these steps:

Install the following dependencies:
NumPy: pip3 install scipy (https://numpy.org/)

Pandas: pip3 install pandas (https://pandas.pydata.org/)

scikit-learn: pip3 install scikit-learn (https://scikit-learn.org/stable/index.html)

Install DIAMOND (https://github.com/bbuchfink/diamond/):

    wget http://github.com/bbuchfink/diamond/releases/download/v2.0.8/diamond-linux64.tar.gz

    tar -xvzf diamond-linux64.tar.gz

Install HMMER (https://www.ebi.ac.uk/Tools/hmmer/search/hmmscan):

    sudo apt-get install hmmer

Install SnakeMake if you want to use the full pipeline provided with CrowdGO: https://snakemake.readthedocs.io/en/stable/

Then follow these steps to set up CrowdGO and the accompanying pipeline for the pre-trained CrowdGOLight-SwissProt models:

    1. git clone https://gitlab.com/mreijnders/CrowdGO.git
    2. cd CrowdGO
    3. tar -xvzf models.tar.gz
    4. gunzip tools/cath-tools-genomescan/data/funfam-hmm3-v4_2_0.lib.gz
    5. tar -xvzf tools/wei2go/data/goaUniprot_IEA.tar.gz --directory tools/wei2go/data/
    6. <path_to_diamond>/diamond makedb --in tools/deepgoplus/data/training_data.fasta --db tools/deepgoplus/data/train_data.dmnd
    7. cd databases
    8. wget https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz (or use one of the mirrors on https://www.uniprot.org/downloads)
    9. gunzip uniprot_sprot.fasta.gz
    10. <path_to_diamond>/diamond makedb --in uniprot_sprot.fasta --db uniprot_sprot
    11. wget http://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz 
    12. gunzip Pfam-A.hmm.gz
    13. hmmpress Pfam-A.hmm

Finally, now that CrowdGO is set up you will have to edit the 'config.yaml' file in the CrowdGO base directory. Assuming you followed the above directions:

    fastaFile: 'test.fasta' ## Path to protein fasta sequences for which to predict GO terms
    ModelFolder: 'models/CrowdGOLight_SwissProt' ## Path to the model used for predicting fasta sequences
    outputFolder: 'example_output' ## Path to the Output folder where all predictions will be written to
    tmpFolder: 'temp' ## Path to a folder where temporary files can be written -> this is relative to the output folder
    knowledgebaseDBPath: 'databases/uniprot_sprot.dmnd' ## Path to the UniProt diamond database
    PfamDBPath: 'databases/Pfam-A.hmm' ## Path to the Pfam-A.hmm hmmscan database

    mode: 'Light-SwissProt' ## Full, Full-SwissProt, Light, Light-SwissProt

    CrowdGOFolder: '.' ## Path where CrowdGO is installed
    DeepGOPlusFolder: 'tools/deepgoplus/' ## Path where DeepGOPlus is installed relative to the CrowdGO path
    FunFamsFolder: 'tools/cath-tools-genomescan/' ## Path where FunFams is installed (cath-genome-tools) relative to the CrowdGO path
    InterProScanFolder: 'tools/interproscan' ## Path where InterProScan is installed relative to the CrowdGO path
    Wei2GOFolder: 'tools/wei2go/' ## Path where Wei2GO is installed relative to the CrowdGO path

    DiamondBinaryPath: 'diamond' ## If the DIAMOND binary is not transfered to a place in the users $PATH, change it to the full path of the DIAMOND binary

To see if the setup was successful, run the SnakeMake pipeline as follows:

    snakemake --cores 1 -s CrowdGO.snakefile

Once finished, the results should be available in the 'example_output' directory. Each respective ontology has their separate folder, containing the CrowdGO annotations with a probability of >= 0.5 (crowdgo.tab) and all the raw annotations (crowdgo.raw.tab).

##  Installing CrowdGO

To install CrowdGO itself, simply clone this gitlab page and install its dependencies. The git project comes with DeepGOPlus, FunFams (cath-tools-genomescan) and Wei2GO. Apart from convenience, as these programs are required for use with the standard CrowdGO models, both DeepGOPlus and FunFams come with slight adjustments to make them compatible with CrowdGO and the snakemake pipeline.

`git clone https://gitlab.com/mreijnders/CrowdGO.git`

##### Dependencies

CrowdGO is written purely in Python3. To install the latest version of Python visit https://www.python.org/downloads/.

The following Python packages are required. They are easiest to install using pip, but can be installed through other methods as detailed on their respective websites.

NumPy: pip3 install scipy (https://numpy.org/)

Pandas: pip3 install pandas (https://pandas.pydata.org/)

scikit-learn: pip3 install scikit-learn (https://scikit-learn.org/stable/index.html)

Optionally, if you want to create your own CrowdGO model (via CrowdGO_training.py), you will need to install the Imbalanced Learn python package: pip3 install imbalanced-learn (https://imbalanced-learn.readthedocs.io/en/stable/api.html)

##### Installation of InterProScan

InterProScan does not come with CrowdGO, and pre-trained models are provided both with and without InterProScan, making it entirely optional.
If not already installed, you can find how to install the latest version here: https://interproscan-docs.readthedocs.io/en/latest/HowToDownload.html. The installation of PANTHER models as described is optional, but recommended.

##### Installation of DIAMOND

DIAMOND is an aligner similar to BLAST, but much faster. It is required for DeepGOPlus and Wei2GO. Installation instructions can be found here: http://www.diamondsearch.org/index.php

###### Installation of the DIAMOND database
Wei2GO requires DIAMOND or BLASTP alignments against the UniProt database. Due to the size of the TrEMBL database (unreviewed proteins) compared to the SwissProt database (reviewed proteins), CrowdGO provides both a pre-trained model where Wei2GO uses TrEMBL and SwissProt alignments, and a pre-trained model where Wei2GO only uses SwissProt alignments. The SwissProt only version gives a slightly reduced precision and recall, but is faster and requires much less space on your hard drive. 

To set up the database first download SwissProt and TrEMBl from https://www.uniprot.org/downloads. Note that using the UK or Switzerland FTP can be significantly faster depending on your location (when using the ftp, download the TrEMBL and/or SwissProt fasta files in databases/uniprot/current_release/knowledgebase/complete/). Then perform the following commands:

    $ cd <folder_containing_uniprot_fasta_files>
    $ gunzip uniprot_sprot.fasta.gz
    $ gunzip uniprot_trembl.fasta.gz
    $ cat uniprot_sprot.fasta >> uniprot_trembl.fasta ## this appends the swissprot proteins to the end of the trembl fasta file
    $ mv uniprot_trembl.fasta knowledgebase.fasta
    $ diamond makedb -d knowledgebase --in knowledgebase.fasta ## this creates the DIAMOND database

##### Installation of HMMER

HMMER (HMMScan / HMMSearch) is required for FunFams and Wei2GO. HMMER can be found here: http://hmmer.org/download.html

To install both HMMScan and HMMSearch in your path, follow these instructions. Note that you might want to use sudo.

    $ cd <hmmer path>
    $ ./configure
    $ make
    $ make check
    $ make install

###### Installation of the HMMER database
Wei2GO requires HMMScan searches on the Pfam-A database. Simply get the latest Pfam-A.hmm.gz file from ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/ and perform the following commands:

    $ cd <folder_containing_Pfam-A_file
    $ gunzip Pfam-A.hmm.gz
    $ hmmpress Pfam-A.hmm

##### Additional data file required for FunFams

FunFams requires a data file too large for storing on github/gitlab. For FunFams do the following:

    $ cd tools/cath-tools-genomescan/data
    $ wget http://download.cathdb.info/cath/releases/all-releases/v4_2_0/sequence-data/funfam-hmm3-v4_2_0.lib.gz
    $ gunzip funfam-hmm3-v4_2_0.lib.gz

Please note that the latest funfams database version (funfam-hmm3-v_3_0.lib) is currently not compatible with CrowdGO due to compatibility issues with HMMER. We will update this page when this is fixed.

##### DeepGOPlus hotfix

This distribution of DeepGOPlus should be compatible with the latest version of DIAMOND. But if you want to make sure DeepGOPlus works correctly, simply do:

    $ cd deepgoplus/data
    $ diamond makedb --in training_data.fasta --db train_data.dmnd
    
## Running CrowdGO using Snakemake

Snakemake is a flexible workflow management tool (instructions to install can be found here https://snakemake.readthedocs.io/en/stable/getting_started/installation.html). For ease-of-use, CrowdGO comes with a Snakemake pipeline to run all the pre-processing of CrowdGO. This means that given a protein fasta file, the Snakemake workflow will run DeepGOPlus, FunFams, Wei2GO, and optionally InterProScan, in one go. Furthermore, some pre-processing is done to get the required CrowdGO input. Once CrowdGO is properly installed, as well as the UniProt and Pfam databases, the user just has to edit the file 'config.yaml' to point the snakemake file in the right direction.

    fastaFile: 'test.fasta' ## Path to protein fasta sequences for which to predict GO terms
    ModelFilePath: 'models/modelFull.pkl' ## Path to the model used for predicting fasta sequences
    outputFolder: 'output' ## Path to the Output folder where all predictions will be written to
    tmpFolder: 'temp' ## Path to a folder where temporary files can be written
    knowledgebaseDBPath: '/path/to/knowledgebase.dmnd' ## Path to the UniProt diamond database
    PfamDBPath: '/path/to/Pfam-A.hmm' ## Path to the Pfam-A.hmm hmmscan database

    CrowdGOFolder: '.' ## Path where CrowdGO is installed
    DeepGOPlusFolder: 'tools/deepgoplus/' ## Path where DeepGOPlus is installed
    FunFamsFolder: 'tools/cath-tools-genomescan/' ## Path where FunFams is installed (cath-genome-tools)
    InterProScanFolder: 'tools/interproscan-<version>' ## Path where InterProScan is installed
    Wei2GOFolder: 'tools/wei2go/' ## Path where Wei2GO is installed

    DiamondBinaryPath: 'diamond' ## If the DIAMOND binary is not transfered to a place in the users $PATH, change it to the full path to the DIAMOND binary
    
Once the config file is set up, all the user has to do is run the following command and wait...

    $ snakemake -s CrowdGO.snakefile
    
Please note that there are four available CrowdGO models to choose from, depending on user limitations.

## Running CrowdGO directly via python

Of course, CrowdGO can be run without the provided Snakemake files. If you wish to do so, perform the following command:

    $ python3 CrowdGO.py -i <input.tab -o <out_dir> -m <model_file>
    
CrowdGO input is a tabular file with the following columns:

    Method_name
    Protein_ID
    GO_term
    Score

Where the method name is the source of the prediction. These have to be exact matches with the model method names (disregarding capitalization). See the model input names for the pre-trained models below.

## CrowdGO pre-trained models

CrowdGO provides four pre-trained models: CrowdGOFull, CrowdGOFull-SwissProt, CrowdGOLight, CrowdGOLight-SwissProt.

CrowdGOFull is trained on the following methods:
    
    DeepGOPlus
    FunFams
    IPRScan
    Wei2GO

CrowdGOLight excludes InterProScan, as this predictor takes significantly longer than the other methods. If you have many proteins and don't mind the slight reduction in accuracy you might want to use this model.

Both '-SwissProt' versions are trained with Wei2GO only utilizing Swissprot data. The TrEMBL database is very large and might not be feasible to set up for all users.

## Training models for CrowdGO

It is possible to create your own model for CrowdGO. This is done using:

    $ python3 CrowdGO_training.py -i <input.tab> -o <output_folder>
    
The CrowdGO input is a tabular file similar to the normal CrowdGO input, with the addition of a 'label' column which specifies if the prediction is a 'True' or 'False' prediction. This enables CrowdGO to learn patterns on whether a prediction is reliable or not.

    Method_name
    Protein_ID
    GO_term
    Score
    Label (True or False)
    
[For training CrowdGO models we *highly* recommend reading this page for instructions and pointers.](https://gitlab.com/mreijnders/CrowdGO/-/wikis/Training-a-model-with-CrowdGO) Additionally, we are always open for suggestions on new tools to incorporate in CrowdGO. Please contact us at maarten.reijnders@unil.ch and we will likely be willing to create a new CrowdGO model for you, provided the tool meets the requirements necessary.
