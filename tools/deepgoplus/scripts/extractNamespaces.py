#!/usr/bin/python3
import sys

for line in open(sys.argv[1]):
	if line.startswith('id: GO:'):
		go = line.strip().split(' ')[1]
	elif line.startswith('namespace: '):
		namespace = line.strip().split(' ')[1]
		if not namespace == 'external':
			print(go+'\t'+namespace)
	elif line.startswith('alt_id'):
		go = line.strip().split(' ')[1]
		print(go+'\t'+namespace)
