##
## Wei2GO: weighted sequence similarity-based protein function prediction
## Author: MJMF Reijnders
##

import sys
from collections import defaultdict
from math import log
from math import log2
from math import log10
import numpy
import os

## Create a dictionary containing all parental relationships of GO terms
def get_go_slim_parents(argotToo_path):
	parents_dict = defaultdict(list)
	go_slim_dict = defaultdict(list)
	for line in open(argotToo_path+'/data/goParents.tab'):
		ssline = line.strip().split('\t')
		child = ssline[0]
		parent = ssline[1]
		if not child == parent:
			parents_dict[child].append(parent)
			go_slim_dict[child].append(parent)
	return(parents_dict,go_slim_dict)

## Create a dictionary containing all children relationships of GO terms
def get_go_slim_children(argotToo_path,go_slim_dict):
	children_dict = defaultdict(list)
	for line in open(argotToo_path+'/data/goChildren.tab'):
		ssline = line.strip().split('\t')
		child = ssline[1]
		parent = ssline[0]
		if not child == parent:
			children_dict[parent].append(child)
			go_slim_dict[parent].append(child)
	return(children_dict,go_slim_dict)

## Get a root node associated to each GO term
def get_root_nodes(argotToo_path):
	root_node_dict = {}
	for line in open(argotToo_path+'/data/nameSpaces.tab'):
		ssline = line.strip().split('\t')
		go_term = ssline[0]
		root_node = ssline[1]
		if root_node == 'biological_process':
			root_node_dict[go_term] = 'GO:0008150'
		elif root_node == 'molecular_function':
			root_node_dict[go_term] = 'GO:0003674'
		elif root_node == 'cellular_component':
			root_node_dict[go_term] = 'GO:0005575'
		else:
			root_node_dict[go_term] = 'GO:0008150'
	root_node_dict['GO:0008150'] = 'GO:0008150'
	root_node_dict['GO:0003674'] = 'GO:0003674'
	root_node_dict['GO:0005575'] = 'GO:0005575'
	return(root_node_dict)

## Create a dictionary containing the number of occurrences of each GO term in the uniprot database
def get_go_counts(argotToo_path,root_node_dict,children_dict):
	go_counts_dict = defaultdict(int)
	for line in open(argotToo_path+'/data/goCounts.tab'):
		go_term,counts = line.strip().split('\t')
		counts = int(counts)
		go_counts_dict[go_term] = counts
		#if go_term in root_node_dict:
			#root_node = root_node_dict[go_term]
			#go_counts_dict[root_node] += counts
	root_list = ['GO:0008150','GO:0003674','GO:0005575']
	for root_node in root_list:
		root_children = children_dict[root_node]
		for root_child in root_children:
			go_counts_dict[root_node] += float(go_counts_dict[root_child])
	go_eligibility_list = go_counts_dict.keys()
	return(go_counts_dict,go_eligibility_list)

## Read the DIAMOND input file and create a dictionary which associates all GO IEA annotations to proteins
def readBlast_IEA(argotToo_path,blast_file_path,go_eligibility_list):
	prot_to_go_dict_blast = defaultdict(dict)
	prot_to_go_dict_goa_pfam = defaultdict(set)
	sseq_set = set([])
	for line in open(blast_file_path):
		ssline = line.strip().split('\t')
		sseq_id = ssline[1]
		sseq_id_short = sseq_id.split('|')[1]
		sseq_set.add(sseq_id_short)
	
	count = 0
	for line in open(argotToo_path+'/data/goaUniprot_IEA.tab'):
		ssline = line.strip().split('\t')
		prot = ssline[0]
		count += 1
		if prot in sseq_set:
			go_term = ssline[1]
			prot_to_go_dict_goa_pfam[prot].add(go_term)
	
	for line in open(blast_file_path):
		ssline = line.strip().split('\t')
		qseq_id = ssline[0]
		sseq_id = ssline[1]
		sseq_id_short = sseq_id.split('|')[1]
		evalue = float(ssline[-2])
		#evalue = float(ssline[2])
		go_list = prot_to_go_dict_goa_pfam[sseq_id_short]
		for go_term in go_list:
			if go_term in go_eligibility_list:
				if evalue <= 1:
					if evalue == 0.0:
						evalue = 2.225074e-308
					weight = abs(-log10(evalue))
					prot_to_go_dict_blast[qseq_id].setdefault(go_term,[]).append([sseq_id,evalue,weight])
	return(prot_to_go_dict_blast,count)

## Read the DIAMOND input file and create a dictionary which associates all GO annotations that are not IEA to proteins
def readBlast_nonIEA(argotToo_path,blast_file_path,go_eligibility_list):
        prot_to_go_dict_blast = defaultdict(dict)
        prot_to_go_dict_goa_pfam = defaultdict(set)
        sseq_set = set([])
        for line in open(blast_file_path):
                ssline = line.strip().split('\t')
                sseq_id = ssline[1]
                sseq_id_short = sseq_id.split('|')[1]
                sseq_set.add(sseq_id_short)

        print('Matching BLAST to GOA')
        count = 0
        for line in open(argotToo_path+'/data/goaUniprot_noIEA.tab'):
                ssline = line.strip().split('\t')
                prot = ssline[0]
                count += 1
                if prot in sseq_set:
                        go_term = ssline[1]
                        prot_to_go_dict_goa_pfam[prot].add(go_term)

        for line in open(blast_file_path):
                ssline = line.strip().split('\t')
                qseq_id = ssline[0]
                sseq_id = ssline[1]
                sseq_id_short = sseq_id.split('|')[1]
                evalue = float(ssline[-2])
                go_list = prot_to_go_dict_goa_pfam[sseq_id_short]
                for go_term in go_list:
                        if go_term in go_eligibility_list:
                                if evalue <= 1:
                                    if evalue == 0.0:
                                            evalue = 2.225074e-308
                                    weight = abs(log10(evalue))
                                    prot_to_go_dict_blast[qseq_id].setdefault(go_term,[]).append([sseq_id,evalue,weight])
        return(prot_to_go_dict_blast,count)

## Read the pfam2go file into a dictionary
def read_pfam2go(argotToo_path):
	pfam2go_dict = defaultdict(list)
	for line in open(argotToo_path+'/data/pfam2go'):
		if not line.startswith('!'):
			ssline = line.strip().split(' ')
			pfam_id = ssline[1]
			go = ssline[-1]
			pfam2go_dict[pfam_id].append(go)
	#for line in open(argotToo_path+'/data/enhancedPfam.tab'):
	#	ssline = line.strip().split('\t')
	#	domain = ssline[0]
	#	go = ssline[1]
	#	pfam2go_dict[domain].append(go)
	return(pfam2go_dict)

def enhance_pfam2go(argotToo_path,pfam2go_dict):
	for line in open(argotToo_path+'/data/enhancedPfam.tab'):
		ssline = line.strip().split('\t')
		domain = ssline[0]
		go = ssline[1]
		pfam2go_dict[domain].append(go)
		if domain == 'RAWUL':
			print(go)
	return(pfam2go_dict)

## Read the HMMER input file into a dictionary associating proteins to GO terms taken from pfam2go
def read_hmmscan(argotToo_path,hmmscan_file_path,go_eligibility_list):
	print('Matching HMMScan to GOA')
	pfam2go_dict = read_pfam2go(argotToo_path)
	prot_to_go_dict_hmmscan = defaultdict(dict)
	pfam2go_dict = read_pfam2go(argotToo_path)
	for line in open(hmmscan_file_path):
		if not line.startswith('#'):
			ssline = ' '.join(line.split()).split(' ')
			#ssline = line.strip().split('\t')
			qseq_id = ssline[2]
			pfam_id = ssline[0]
			evalue = float(ssline[4])
			if evalue <= 1:
				if evalue < 1e250:
					evalue = 2.225074e-308
				go_list = pfam2go_dict[pfam_id]
				if not go_list == []:
					weight = abs(-log10(evalue))
					for go_term in go_list:
						if go_term in go_eligibility_list:
							prot_to_go_dict_hmmscan[qseq_id].setdefault(go_term,[]).append([pfam_id,evalue,weight])
		
	return(prot_to_go_dict_hmmscan)

## Merge the previously created dictionary structures for BLAST with IEA terms, BLAST without IEA terms, and HMMSCAN with pfam terms into one merged dictionary associating proteins to GO terms
def merge_annotations(prot_to_go_dict_blast_IEA,prot_to_go_dict_blast_nonIEA,prot_to_go_dict_hmmscan,iea_count,nonIEA_count):
	print('Merging BLAST and HMMScan annotations')
	prot_to_go_dict = defaultdict(dict)
	modifier = log2(iea_count/nonIEA_count)
	for prot,go_terms in prot_to_go_dict_blast_IEA.items(): ## Iterate over the BLAST with IEA terms dictionary, and merge with BLAST non-IEA GO terms and HMMER GO terms
		for go_term in go_terms:
			total_weight = 0
			hmmscan_hits = []
			nonIEA_hits = []
			blast_hits = []
			blast_hits = prot_to_go_dict_blast_IEA[prot][go_term]
			for sseq_id in prot_to_go_dict_blast_IEA[prot][go_term]:
				weight = sseq_id[-1]
				total_weight += weight
			if prot in prot_to_go_dict_blast_nonIEA:
				if go_term in prot_to_go_dict_blast_nonIEA[prot]:
					for sseq_id in prot_to_go_dict_blast_nonIEA[prot][go_term]:
						weight = sseq_id[-1]
						total_weight += weight*modifier
					nonIEA_hits = prot_to_go_dict_blast_nonIEA[prot][go_term]
			if prot in prot_to_go_dict_hmmscan:
				if go_term in prot_to_go_dict_hmmscan[prot]:
					for sseq_id in prot_to_go_dict_hmmscan[prot][go_term]:
						weight = sseq_id[-1]
						total_weight += weight#*modifier
					hmmscan_hits = prot_to_go_dict_hmmscan[prot][go_term]
			try:
				prot_to_go_dict[prot].setdefault(go_term,[]).append(total_weight)
				prot_to_go_dict[prot][go_term].append(blast_hits)
				prot_to_go_dict[prot][go_term].append(nonIEA_hits)
				prot_to_go_dict[prot][go_term].append(hmmscan_hits)
			except:
				continue

	for prot,go_terms in prot_to_go_dict_blast_nonIEA.items(): ## Iterate over the remaining proteins in the BLAST non-IEA dictionary and merge with HMMER GO terms
		for go_term in go_terms:
			if not go_term in prot_to_go_dict[prot]:
				total_weight = 0
				nonIEA_hits = prot_to_go_dict_blast_nonIEA[prot][go_term]
				hmmscan_hits = []
				for sseq_id in nonIEA_hits:
					weight = sseq_id[-1]
					total_weight += weight*modifier
				if prot in prot_to_go_dict_hmmscan:
					if go_term in prot_to_go_dict_hmmscan[prot]:
						for sseq_id in prot_to_go_dict_hmmscan[prot][go_term]:
							weight = sseq_id[-1]
							total_weight += weight#*modifier
						hmmscan_hits = prot_to_go_dict_hmmscan[prot][go_term]
				try:
					prot_to_go_dict[prot].setdefault(go_term,[]).append(total_weight)
					prot_to_go_dict[prot][go_term].append([])
					prot_to_go_dict[prot][go_term].append(nonIEA_hits)
					prot_to_go_dict[prot][go_term].append(hmmscan_hits)
				except:
					continue

	for prot,go_terms in prot_to_go_dict_hmmscan.items(): ## Iterate over the remaining HMMER proteins and add to dictionary as solo entries, since the other dictionaries are now empty
		for go_term in go_terms:
			if not go_term in prot_to_go_dict[prot]:
				total_weight = 0
				hmmscan_hits = prot_to_go_dict_hmmscan[prot][go_term]
				for sseq_id in hmmscan_hits:
					weight = float(sseq_id[-1])
					total_weight += weight#*modifier
				try:
					prot_to_go_dict[prot].setdefault(go_term,[]).append(total_weight)
					prot_to_go_dict[prot][go_term].append([])
					prot_to_go_dict[prot][go_term].append([])
					prot_to_go_dict[prot][go_term].append(hmmscan_hits)
				except:
					continue
	return(prot_to_go_dict)

## For each protein-GO association, get the sum of the weights across all hits (IEA, non-IEA, HMMER)
## Each weight_dict entry is a [protein] containing a [go term] containing a weight
## Root nodes are updated by summing the final weight of each GO term
def get_go_weight_dict(prot_to_go_dict, children_dict, root_node_dict):
	weight_dict = defaultdict(dict)
	for prot,go_terms in prot_to_go_dict.items():
		go_term_checklist_dict = defaultdict(list) ## Dictionary to make sure there GO weights arent added twice to the root weight
		for go_term in go_terms:
			if go_term in root_node_dict:
				root_node = root_node_dict[go_term]
			else: ## If for some reason the root node dictionary doesn't contain the GO term, it is added to the BP root node as this is the biggest one
				root_node_dict[go_term] = 'GO:0008150' 
				root_node = 'GO:0008150'
			total_weight = float(prot_to_go_dict[prot][go_term][0]) ## Summed weight of all GO term hits 
			weight_dict[prot].setdefault(go_term,0) ## Create a new dictionary containing the weights
			weight_dict[prot][go_term] += total_weight
			for go_term2 in go_terms: ## Loop that adds the GO weights of all child terms to the weight dict of a GO term
				if go_term2 in children_dict[go_term] and not go_term2 in go_term_checklist_dict[go_term]:
					total_weight2 = float(prot_to_go_dict[prot][go_term][0])
					weight_dict[prot][go_term] += total_weight2
					go_term_checklist_dict[go_term].append(go_term2)
			if not go_term in go_term_checklist_dict[root_node]: ## If the GO's weight (without children added up) hasn't been added to the root weight yet, add it.
				weight_dict[prot].setdefault(root_node,0)
				weight_dict[prot][root_node] += total_weight
				go_term_checklist_dict[root_node].append(go_term)
	return(weight_dict)

## Calculates the internal confidence score and returns a dictionary associating each protein go term pair to a score
def get_inc_scores(weight_dict,root_node_dict):
	print('Calculating Inc scores')
	inc_score_dict = defaultdict(dict)
	for prot,go_terms in weight_dict.items():
		for go_term in go_terms:
			if not go_term in root_node_dict:
				root_node = 'GO:0008150'
			else:
				root_node = root_node_dict[go_term]
			go_weight = weight_dict[prot][go_term]
			root_go_weight = weight_dict[prot][root_node]
			inc_score = go_weight / root_go_weight
			inc_score_dict[prot][go_term] = inc_score
	return(inc_score_dict)

## Calculates the IC score for a GO term
def get_ic_score(go_term,go_counts_dict,root_node_dict,children_dict):
	if go_term in root_node_dict:
		root_node = root_node_dict[go_term]
	else:
		root_node = 'GO:0008150'
	total_root_proteins = go_counts_dict[root_node]
	ic = 0
	go_counts = float(go_counts_dict[go_term])
	children = children_dict[go_term]
	for child in children:
		go_counts += float(go_counts_dict[child])
	if go_counts == 0:
		go_counts = 1
	ic = -log10(float(go_counts)/float(total_root_proteins))
	return(ic)

## Calculate the semantic similarity between two GO terms
def get_semantic_similarity(go_term_1,go_term_2,parents_dict,root_node_dict,children_dict,go_counts_dict):
	go_term_1_ic = get_ic_score(go_term_1,go_counts_dict,root_node_dict,children_dict)
	go_term_2_ic = get_ic_score(go_term_2,go_counts_dict,root_node_dict,children_dict)
	go_term_1_parents = parents_dict[go_term_1]
	go_term_2_parents = parents_dict[go_term_2]
	if go_term_1_ic > go_term_2_ic:
		highest_ic = go_term_1_ic
		highest_ic_term = go_term_1
	else:
		highest_ic = go_term_2_ic
		highest_ic_term = go_term_2
	semantic_similarity = (2*highest_ic)/(go_term_1_ic+go_term_2_ic)
	return(semantic_similarity)

## Calculate the group score for each protein go term pair
def get_group_scores(inc_dict,parents_dict,root_node_dict,children_dict,go_counts_dict):
	print('Calculating group scores')
	group_score_dict = defaultdict(dict)
	group_score_nc_dict = defaultdict(dict)
	for prot,go_terms in inc_dict.items():
		for go_term in go_terms:
			inc_score = float(inc_dict[prot][go_term])
			group_score_dict[prot].setdefault(go_term,0)
			group_score_dict[prot][go_term] += inc_score
			group_score_nc_dict[prot].setdefault(go_term,0)
			group_score_nc_dict[prot][go_term] += inc_score
			go_parents = parents_dict[go_term]
			for go_parent in go_parents:
				semantic_similarity = get_semantic_similarity(go_term,go_parent,parents_dict,root_node_dict,children_dict,go_counts_dict)
				if semantic_similarity >= 0.7: ## Similar proteins for a group currently set at 0.7
					group_score_dict[prot].setdefault(go_parent,0)
					group_score_dict[prot][go_parent] += inc_score
	return(group_score_dict,group_score_nc_dict)

## Standard deviation function for calculating 
def get_standard_deviation(weight_dict,group_score_dict):
	list = []
	for prot,go_terms in weight_dict.items():
		for go_term in go_terms:
			list.append(group_score_dict[prot][go_term])
	standard_deviation = numpy.std(list)
	return(standard_deviation)

## Calculation of the total scores for each protein go term pair, and writes the predictions to the output file
def get_total_scores(prot_to_go_dict,inc_score_dict,go_counts_dict,root_node_dict,children_dict,go_slim_dict):
	outfile = open(sys.argv[3],'w')
	outfile.write('Protein\tGO term\tScore\n')
	outfile_all = open(sys.argv[3]+'.all_preds','w')
	outfile_all.write('Protein\tGO term\tScore\n')
	for prot,go_terms in prot_to_go_dict.items():
		for go_term in go_terms:
			if go_term in root_node_dict and not go_term == 'GO:0008150' and not go_term == 'GO:0005575' and not go_term == 'GO:0003674':
				go_weight = prot_to_go_dict[prot][go_term][0]
				inc_score = inc_score_dict[prot][go_term]
				group_score = group_score_dict[prot][go_term]
				ic_score = get_ic_score(go_term,go_counts_dict,root_node_dict,children_dict)
				inc_group_score = inc_score/group_score
				total_score = ic_score*inc_group_score*go_weight
				if total_score >= 500:
					outfile.write(prot+'\t'+go_term+'\t'+str(total_score)+'\n')
				outfile_all.write(prot+'\t'+go_term+'\t'+str(total_score)+'\n')
	outfile.close()
	outfile_all.close()
	return

argotToo_path = os.path.dirname(os.path.abspath(__file__))
blast_file_path = sys.argv[1]
hmmscan_file_path = sys.argv[2]
parents_dict,go_slim_dict = get_go_slim_parents(argotToo_path)
children_dict,go_slim_dict = get_go_slim_children(argotToo_path,go_slim_dict)
root_node_dict = get_root_nodes(argotToo_path)
go_counts_dict,go_eligibility_list = get_go_counts(argotToo_path,root_node_dict,children_dict)
prot_to_go_dict_blast_IEA,iea_count = readBlast_IEA(argotToo_path,blast_file_path,go_eligibility_list)
prot_to_go_dict_blast_nonIEA,nonIEA_count = readBlast_nonIEA(argotToo_path,blast_file_path,go_eligibility_list)
prot_to_go_dict_hmmscan = read_hmmscan(argotToo_path,hmmscan_file_path,go_eligibility_list)
prot_to_go_dict = merge_annotations(prot_to_go_dict_blast_IEA,prot_to_go_dict_blast_nonIEA,prot_to_go_dict_hmmscan,iea_count,nonIEA_count)
weight_dict = get_go_weight_dict(prot_to_go_dict,parents_dict,root_node_dict)
inc_score_dict = get_inc_scores(weight_dict,root_node_dict)
group_score_dict,group_score_nc_dict = get_group_scores(inc_score_dict,parents_dict,root_node_dict,children_dict,go_counts_dict)
standard_deviation = get_standard_deviation(weight_dict,group_score_dict)
get_total_scores(prot_to_go_dict,inc_score_dict,go_counts_dict,root_node_dict,children_dict,go_slim_dict)
