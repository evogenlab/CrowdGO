#!/usr/bin/env python3
import sys
from collections import defaultdict

predsDict = defaultdict(list)
predList = []

namespaceDict = defaultdict(str)
for line in open(sys.argv[1]):
        go,namespace = line.strip().split('\t')
        if not go.startswith('GO:'):
                go = 'GO:'+go
        namespaceDict[go] = namespace

namespace = sys.argv[6]

for line in open(sys.argv[2]):
	if not line.startswith('Protein'):
		prot,go,score = line.strip().split('\t')
		if namespaceDict[go] == namespace and float(score) > 500:
			predsDict['Wei2GO'].append([prot,go,score])
			predList.append([prot,go])
			print('Wei2GO\t'+prot+'\t'+go+'\t'+score)

for line in open(sys.argv[3]):
	prot,go,score = line.strip().split('\t')
	if namespaceDict[go] == namespace and float(score) >= 0.2:
		predsDict['DeepGOPlus'].append([prot,go,score])
		predList.append([prot,go])
		print('DeepGOPlus\t'+prot+'\t'+go+'\t'+score)

for line in open(sys.argv[4]):
	prot,go,score = line.strip().split('\t')
	if namespaceDict[go] == namespace and float(score) >= 0.8:
		predsDict['FunFams'].append([prot,go,score])
		predList.append([prot,go])
		print('FunFams\t'+prot+'\t'+go+'\t'+score)

for line in open(sys.argv[5]):
	prot,go,score = line.strip().split('\t')
	if namespaceDict[go] == namespace:
		predsDict['IPRScan'].append([prot,go,score])
		predList.append([prot,go])
		print('IPRScan\t'+prot+'\t'+go+'\t'+score)

if len(predsDict['Wei2GO']) == 0:
	prot,go = predList[0]
	print('Wei2GO'+'\t'+prot+'\t'+go+'\t0')

if len(predsDict['DeepGOPlus']) == 0:
	prot,go = predList[0]
	print('DeepGOPlus'+'\t'+prot+'\t'+go+'\t0')

if len(predsDict['FunFams']) == 0:
	prot,go = predList[0]
	print('FunFams'+'\t'+prot+'\t'+go+'\t0')

if len(predsDict['IPRScan']) == 0:
	prot,go = predList[0]
	print('IPRScan'+'\t'+prot+'\t'+go+'\t0')
