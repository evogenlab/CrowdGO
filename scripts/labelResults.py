#!/usr/bin/env python3
import sys
from collections import defaultdict
import argparse

parser = argparse.ArgumentParser(description='Calculate Fmax')
parser.add_argument('-i','--input',help='Input file',required=True,action='store')
parser.add_argument('-t','--testset',help='Testset file',required=True,action='store')
parser.add_argument('-r','--relations',help='GO relations file',required=True,action='store')
parser.add_argument('-o','--output',help='Output file',required=True,action='store')
args = parser.parse_args()
inputFilePath = args.input
testsetFilePath = args.testset
goRelationsFilePath = args.relations
outputFilePath = args.output

goRelationDict = defaultdict(list)
for line in open(goRelationsFilePath):
	go1,go2 = line.strip().split('\t')
	goRelationDict[go1].append(go2)
	goRelationDict[go2].append(go1)

trueDict = defaultdict(list)
for line in open(testsetFilePath):
	protein,go = line.strip().split('\t')
	trueDict[protein].append(go)

outFile = open(outputFilePath,'w')
outFile.write('Protein\tGO term\tScore\tLabel\n')
for line in open(inputFilePath):
	if not line.startswith('Protein'):
		prot,go,score = line.strip().split('\t')
		label = 'False'
		if go in trueDict[prot]:
			label = 'True'
		else:
			for relatedGo in goRelationDict[go]:
				if relatedGo in trueDict[prot]:
					label = 'True'
		outFile.write(line.strip()+'\t'+label+'\n')
outFile.close()
