#!/usr/bin/env python3
import sys
import numpy
import argparse

parser = argparse.ArgumentParser(description='Calculate Fmax')
parser.add_argument('-i','--input',help='Input file',required=True,action='store')
parser.add_argument('-m','--max',help='Maximum score cutoff',required=True,action='store')
parser.add_argument('-n','--min',help='Minimum score cutoff',required=True,action='store')
parser.add_argument('-s','--steps',help='Score cutoff step size',required=True,action='store')
parser.add_argument('-t','--total',help='Total GO terms in test set',required=True,action='store')
args = parser.parse_args()
inputFilePath = args.input
cutoffMax = float(args.max)
cutoffMin = float(args.min)
cutoffSteps = float(args.steps)
total = int(args.total)
fmax = 0
fmaxCutoff = 0
for i in numpy.arange(cutoffMin,cutoffMax,cutoffSteps):
	tpCount = 0
	fpCount = 0
	tnCount = 0
	fnCount = 0
	cutoff = i
	for line in open(inputFilePath):
		if not line.startswith('Protein'):
			prot,go,score,label = line.strip().split('\t')
			if float(score) >= float(cutoff):
				if label == 'True':
					tpCount += 1
				if label == 'False':
					fpCount += 1
			else:
				if label == 'False':
					tnCount += 1
				elif label == 'True':
					fnCount += 1
	if not tpCount == 0 and not fpCount == 0 and not fnCount == 0:
		missing = total-(tpCount+fnCount)
		fnCount += missing
		precision = tpCount/(tpCount+fpCount)
		recall = tpCount/(tpCount+fnCount)

		fscore = 2*((precision*recall)/(precision + recall))
		if fscore > fmax:
			fmax = fscore
			fmaxCutoff = cutoff
	
print('Fmax: '+str(fmax))
print('Fmax associated cutoff: '+str(fmaxCutoff))
