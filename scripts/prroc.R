library(PRROC)

args <- commandArgs(TRUE)

iprscanPath <- args[5]
argot2Path <- args[4]
crowdgoPath <- args[1]
crowdgo2Path <- args[2]
icscoresPath <- args[7]
deepgoplusPath <- args[6]
crowdgoUnfilteredPath <- args[3]
testPath <- args[8]

crowdgoData <- read.table(crowdgoPath,header=FALSE)
crowdgoScores <- crowdgoData$V1
crowdgoLabels <- crowdgoData$V2
crowdgoPROC <- pr.curve(scores.class0=crowdgoScores,weights.class0=crowdgoLabels,curve=TRUE,sorted=FALSE)

argot2Data <- read.table(argot2Path,header=FALSE)
argot2Scores <- argot2Data$V1
argot2Labels <- argot2Data$V2
argot2PROC <- pr.curve(scores.class0=argot2Scores,weights.class0=argot2Labels,curve=TRUE)

iprscanData <- read.table(iprscanPath,header=FALSE)
iprscanScores <- iprscanData$V1
iprscanLabels <- iprscanData$V2
iprscanPROC <- pr.curve(scores.class0=iprscanScores,weights.class0=iprscanLabels,curve=TRUE)

icscoresData <- read.table(icscoresPath,header=FALSE)
icscoresScores <- icscoresData$V1
icscoresLabels <- icscoresData$V2
icscoresPROC <- pr.curve(scores.class0=icscoresScores,weights.class0=icscoresLabels,curve=TRUE)

deepgoplusData <- read.table(deepgoplusPath,header=FALSE)
deepgoplusScores <- deepgoplusData$V1
deepgoplusLabels <- deepgoplusData$V2
deepgoplusPROC <- pr.curve(scores.class0=deepgoplusScores,weights.class0=deepgoplusLabels,curve=TRUE)

crowdgo2Data <- read.table(crowdgo2Path,header=FALSE)
crowdgo2Scores <- crowdgo2Data$V1
crowdgo2Labels <- crowdgo2Data$V2
crowdgo2PROC <- pr.curve(scores.class0=crowdgo2Scores,weights.class0=crowdgo2Labels,curve=TRUE)

crowdgoUnfilteredData <- read.table(crowdgoUnfilteredPath,header=FALSE)
crowdgoUnfilteredScores <- crowdgoUnfilteredData$V1
crowdgoUnfilteredLabels <- crowdgoUnfilteredData$V2
crowdgoUnfilteredPROC <- pr.curve(scores.class0=crowdgoUnfilteredScores,weights.class0=crowdgoUnfilteredLabels,curve=TRUE)


testData <- read.table(testPath,header=FALSE)
testScores <- testData$V1
testLabels <- testData$V2
testPROC <- pr.curve(scores.class0=testScores,weights.class0=testLabels,curve=TRUE)

pdf('prroc.pdf')
plot(argot2PROC,col='aquamarine3',auc.main=FALSE,main='Precision recall curve of GO term predictions')
plot(crowdgoPROC,col='dodgerblue3',add=TRUE)
plot(crowdgo2PROC,col='firebrick',add=TRUE)
plot(crowdgoUnfilteredPROC,col='darkslateblue',add=TRUE)
plot(iprscanPROC,col='chocolate2',add=TRUE)
plot(icscoresPROC,col='cornsilk4',add=TRUE)
plot(deepgoplusPROC,col='khaki',add=TRUE)
plot(testPROC,col='black',add=TRUE)
legend( "topright", c("CrowdGO 3", "CrowdGO 2", "CrowdGO unfiltered", "Argot2.5", "InterProScan", "DeepGOPlus", "Information Content scores"), 
text.col=c("dodgerblue3", "firebrick", "darkslateblue", "aquamarine", "chocolate2", "khaki", "cornsilk4"))
dev.off()
