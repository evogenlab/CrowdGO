configfile: 'config_training.yaml'

from datetime import date
from datetime import datetime
current_time = datetime.now().strftime("%H-%M-%S")
current_date = str(date.today())

jobName = current_date+'-'+current_time
config['jobName'] = jobName
print("Job name = "+jobName)

if not os.path.isdir(config['tmpFolder']):
	os.mkdir(config['tmpFolder'])

if not os.path.isdir(config['tmpFolder']+'/'+jobName):
	os.mkdir(config['tmpFolder']+'/'+jobName)

config['tmpFolder'] = config['tmpFolder']+'/'+jobName

def inputFunctionFinal(wildcard):
	inputList = []
	if config['DeepGOPlus'] == 'True':
		inputList.append(config['outputFolder']+'/deepgoplus.tab')
	if config['FunFams'] == 'True':
		inputList.append(config['outputFolder']+'/funfams.tab')
	if config['InterProScan'] == 'True':
		inputList.append(config['outputFolder']+'/interproscan.tab')
	if config['Wei2GO'] == 'True':
		inputList.append(config['outputFolder']+'/wei2go.tab')
	inputList.append(config['outputFolder']+'/training_mf/model.pkl')
	inputList.append(config['outputFolder']+'/training_bp/model.pkl')
	inputList.append(config['outputFolder']+'/training_cc/model.pkl')
	return(inputList)

def inputFunctionCatPredictions(wildcard):
	inputList = []
	if config['DeepGOPlus'] == 'True':
		inputList.append(config['outputFolder']+'/deepgoplus.tab')
	if config['FunFams'] == 'True':
		inputList.append(config['outputFolder']+'/funfams.tab')
	if config['InterProScan'] == 'True':
		inputList.append(config['outputFolder']+'/interproscan.tab')
	if config['Wei2GO'] == 'True':
		inputList.append(config['outputFolder']+'/wei2go.tab')
	return(inputList)

def catPreds(input,output):
	outFilePath = str(list(output)[0])
	outfile = open(outFilePath,'w')
	for inputPart in str(list(input)[0]).split(' '):
		if inputPart.endswith('deepgoplus.tab'):
			for line in open(inputPart):
				if not line.startswith('Protein'):
					outfile.write('DeepGOPlus\t'+line)
		elif inputPart.endswith('funfams.tab'):
			for line in open(inputPart):
				if not line.startswith('Protein'):
					outfile.write('FunFams\t'+line)
		elif inputPart.endswith('interproscan.tab'):
			for line in open(inputPart):
				if not line.startswith('Protein'):
					outfile.write('InterProScan\t'+line)
		elif inputPart.endswith('wei2go.tab'):
			for line in open(inputPart):
				if not line.startswith('Protein'):
					outfile.write('Wei2GO\t'+line)
	for line in open(str(list(input)[0]).split(' ')[-1]):
		if not line.startswith('Protein'):
			outfile.write(line)
	outfile.close()

rule final:
	input: inputFunctionFinal
	

if config['DeepGOPlus'] == 'True':
	rule DeepGOPlus:
		input: fastaFile = config['fastaFile']
		output: config['outputFolder']+'/deepgoplus.tab'
		params: DeepGOPlusFolder = config['DeepGOPlusFolder'],
			tmpFolder = config['tmpFolder'],
			CrowdGOFolder = config['CrowdGOFolder']
		run:
			shell("diamond blastp -d {params.DeepGOPlusFolder}/data/train_data.dmnd -q {input} --outfmt 6 qseqid sseqid bitscore > {params.tmpFolder}/deepgoplus.diamond.tab")
			shell("python3 {params.DeepGOPlusFolder}/predict.py -if {input.fastaFile} -of {params.tmpFolder}/deepgoplus.out -df {params.tmpFolder}/deepgoplus.diamond.tab")
			shell("{params.CrowdGOFolder}/scripts/fixDeepGO.py {params.tmpFolder}/deepgoplus.out > {output}")

if config['FunFams'] == 'True':
	rule FunFams:
		input: fastaFile =  config['fastaFile']
		output: config['outputFolder']+'/funfams.tab'
		params: FunFamsFolder = config['FunFamsFolder'],
			CrowdGOFolder = config['CrowdGOFolder'],
			tmpFolder = config['tmpFolder'],
			jobName = config['jobName']
		run:
			shell("{params.FunFamsFolder}/apps/cath-genomescan.pl -i {input} -l {params.FunFamsFolder}/data/funfam-hmm3-v4_2_0.lib -o {params.tmpFolder}/funfams/")
			shell("mkdir {params.tmpFolder}/funfams/funfams_data/")
			shell("{params.CrowdGOFolder}/scripts/retrieveGoForFunFam.py {params.tmpFolder}/funfams/*crh > {params.tmpFolder}/funfams/funfams.domains.tab")
			shell("{params.CrowdGOFolder}/scripts/funfam2go.py {params.tmpFolder}/funfams/*crh {params.tmpFolder}/funfams/funfams_data {params.FunFamsFolder}")
			shell("{params.CrowdGOFolder}/scripts/toGo2.py {params.tmpFolder}/funfams/funfams.domains.tab {params.tmpFolder}/funfams/funfams_data/ > {output}")

if config['InterProScan'].upper() == 'TRUE':
	rule InterProScan:
		input: fastaFile = config['fastaFile']
		output: config['outputFolder']+'/interproscan.tab'
		params: InterProScanFolder = config['InterProScanFolder'],
			CrowdGOFolder = config['CrowdGOFolder'],
			tmpFolder = config['tmpFolder']
		run:
			shell("{params.InterProScanFolder}/interproscan.sh --goterms -i {input} -o {params.tmpFolder}/interproscan.full.tab -f tsv")
			shell("{params.CrowdGOFolder}/scripts/filterIPR.py {params.tmpFolder}/interproscan.full.tab > {output}")

if config['Wei2GO'].upper() == 'TRUE':
	rule DIAMOND:
		input: fastaFile = config['fastaFile']
		output: diamondFile = config['outputFolder']+'/diamond.tab'
		params: diamondBinary = config['DiamondBinaryPath'],
			knowledgebaseDBPath = config['knowledgebaseDBPath']
		shell: "{params.diamondBinary} blastp --query {input} --out {output} --db {params.knowledgebaseDBPath}"

	rule HMMScan:
		input: fastaFile = config['fastaFile']
		output: hmmscanFile = config['outputFolder']+'/hmmscan.out'
		params: pfamDBPath = config['PfamDBPath']
		shell: "hmmscan --tblout {output} {params.pfamDBPath} {input}"

	rule Wei2GO:
		input: diamondFile = config['outputFolder']+'/diamond.tab',
			hmmscanFile = config['outputFolder']+'/hmmscan.out'
		output: config['outputFolder']+'/wei2go.tab'
		params: Wei2GOFolder = config['Wei2GOFolder']
		shell: "python3 {params.Wei2GOFolder}/wei2go.py {input.diamondFile} {input.hmmscanFile} {output}"

rule catPredictions:
	input: inputFunctionCatPredictions,
		newPreds = config['newPredsPath']
	output: config['outputFolder']+'/crowdgo_input_training.tab'
	run:
		catPreds({input},{output})

rule categorizeGOTerms:
	input: config['outputFolder']+'/crowdgo_input_training.tab'
	output: mf = config['outputFolder']+'/crowdgo_input_training.mf.tab',
		bp = config['outputFolder']+'/crowdgo_input_training.bp.tab',
		cc = config['outputFolder']+'/crowdgo_input_training.cc.tab'
	params: CrowdGOFolder = config['CrowdGOFolder']
	run:
		shell("{params.CrowdGOFolder}/scripts/subcatOnly.py {params.CrowdGOFolder}/data/nameSpaces.tab {input} molecular_function > {output.mf}")
		shell("{params.CrowdGOFolder}/scripts/subcatOnly.py {params.CrowdGOFolder}/data/nameSpaces.tab {input} biological_process > {output.bp}")
		shell("{params.CrowdGOFolder}/scripts/subcatOnly.py {params.CrowdGOFolder}/data/nameSpaces.tab {input} cellular_component > {output.cc}")

rule labelpredictions:
	input: mf = config['outputFolder']+'/crowdgo_input_training.mf.tab',
		bp = config['outputFolder']+'/crowdgo_input_training.bp.tab',
		cc = config['outputFolder']+'/crowdgo_input_training.cc.tab'
	output: mf = config['outputFolder']+'/crowdgo_input_training.labeled.mf.tab',
		bp = config['outputFolder']+'/crowdgo_input_training.labeled.bp.tab',
		cc = config['outputFolder']+'/crowdgo_input_training.labeled.cc.tab'
	params: CrowdGOFolder = config['CrowdGOFolder']
	run: 
		shell("{params.CrowdGOFolder}/scripts/label.py -i {input.mf} -o {output.mf} -t {params.CrowdGOFolder}/testset/testset.tab"),
		shell("{params.CrowdGOFolder}/scripts/label.py -i {input.bp} -o {output.bp} -t {params.CrowdGOFolder}/testset/testset.tab"),
		shell("{params.CrowdGOFolder}/scripts/label.py -i {input.cc} -o {output.cc} -t {params.CrowdGOFolder}/testset/testset.tab")

rule CrowdGO_training:
	input: mf = config['outputFolder']+'/crowdgo_input_training.labeled.mf.tab',
		bp = config['outputFolder']+'/crowdgo_input_training.labeled.bp.tab',
		cc = config['outputFolder']+'/crowdgo_input_training.labeled.cc.tab'
	output: mfFolder = config['outputFolder']+'/training_mf/',
		bpFolder = config['outputFolder']+'/training_bp/',
		ccFolder = config['outputFolder']+'/training_cc/',
		mfModel = config['outputFolder']+'/training_mf/model.pkl',
		bpModel =  config['outputFolder']+'/training_bp/model.pkl',
		ccModel =  config['outputFolder']+'/training_cc/model.pkl'
	params: CrowdGOFolder = config['CrowdGOFolder']
	run: 
		shell("python3 {params.CrowdGOFolder}/CrowdGO_training.py -i {input.mf} -o {output.mfFolder}"),
		shell("python3 {params.CrowdGOFolder}/CrowdGO_training.py -i {input.bp} -o {output.bpFolder}"),
		shell("python3 {params.CrowdGOFolder}/CrowdGO_training.py -i {input.cc} -o {output.ccFolder}")
